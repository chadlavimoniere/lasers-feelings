# Lasers and Feelings interactive character sheet

This is an interactive character sheet for playing Lasers & Feelings.
Note that there is no backend to this app, it just uses localstorage.

You can use the app at <https://chadlavimoniere.gitlab.io/lasers-feelings/>.

## Dev

### Developing locally

This uses the [Tailwind CSS standalone CLI](https://tailwindcss.com/blog/standalone-cli) to build and minify CSS.
The binary is in this directory -- note that the version here is the macOS ARM version.

While working on the app locally, make sure this is running in this directory:

```sh
scripts/dev 
```

(Note: you'll need to <kbd>Ctrl</kbd>+<kbd>c</kbd> to end the dev script)

The app will be available at <http://localhost:8000>

Everything in the `publish` folder will be published to the GitLab Pages site.

### Building

Before pushing to `main`, run this:

```sh
scripts/build
```
